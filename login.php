<?php
session_start();
?>

<html>
<title> Login Page </title>
<body bgcolor="#45e1b7">
<body>
<h1>LOGIN PAGE</h1>

<script>

function WrongCreds()
{
	alert("Wrong credentials!!!");
}

function validate() 
{
	var x = document.forms["loginform"]["name"].value;
	var y = document.forms["loginform"]["pass"].value;
	if (x == "" && y == "") 
	{
        	alert("Name and Password must be filled out");
        	return false;
    	}	
	if (x == "") 
	{
        	alert("Name must be filled out");
        	return false;
    	}
	if (y == "") 
	{
        	alert("Password must be filled out");
        	return false;
    	}
}
</script>

<form name = "loginform" action = "logincheck.php" onsubmit = "return validate()" method = "post">
Name:<input type = "text" name = "name" ><br>
Password: <input type = "password" name = "pass" ><br>
<input type = "submit" value = "Login">
</form>

<form action = "registerpage.php" method = "post">
<br><br><br>NEW HERE?<br> <input type = "submit" value = "Click to Register">
</form>

<?php

if ($_SESSION['check'] == 'False')
{
	    echo "<script type='text/javascript'>WrongCreds();</script>";	
}

session_unset();
session_destroy();

?>

</body>
</html>
